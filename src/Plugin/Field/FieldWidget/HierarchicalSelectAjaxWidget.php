<?php

namespace Drupal\hierarchical_select_ajax\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hierarchical_select_ajax\HierarchicalSelectHelper;

/**
 * Provides "hierarchical_select_ajax" field widget.
 *
 * @FieldWidget(
 *   id = "hierarchical_select_ajax",
 *   label = @Translation("Hierarchical select ajax"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class HierarchicalSelectAjaxWidget extends EntityReferenceAutocompleteWidget {

  use HierarchicalSelectHelper {
    defaultSettings as helperDefaultSettings;
    formElement as helperFormElement;
    settingsForm as helperSettingsForm;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    if ($field_definition->getFieldStorageDefinition()->getSetting('target_type') === 'taxonomy_term') {
      /* @see \Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection */
      if (!empty($field_definition->getSettings()['handler_settings']['target_bundles'] ?? [])) {
        return TRUE;
      }

      \Drupal::messenger()->addWarning(\t('The ajax hierarchical select widget cannot be used for the %field because it is not using the default entity reference selection handler or has no taxonomy vocabularies selected.', [
        '%label' => $field_definition->getLabel(),
        // Some fields have no IDs and only names.
        '%field' => \str_replace('.', ' -> ', \method_exists($field_definition, 'id') ? $field_definition->id() : $field_definition->getName()),
      ]));
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return static::helperDefaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return $this->helperSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $hsa_element = parent::formElement($items, $delta, $element, $form, $form_state);
    return $this->helperFormElement($hsa_element);
  }

}
