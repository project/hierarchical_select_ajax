<?php

namespace Drupal\hierarchical_select_ajax\Element;

use Drupal\Core\Render\Element\Select;

/**
 * Defines the hierarchical_select_ajax element.
 *
 * @FormElement("hierarchical_select_ajax")
 */
class HierarchicalSelectElement extends Select {

  public const ID = 'hierarchical_select_ajax';
  public const NONE_VALUE = '_none';
  public const NONE_LABEL = '- Please select -';

}
