<?php

namespace Drupal\hierarchical_select_ajax;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hierarchical_select_ajax\Element\HierarchicalSelectElement;
use Drupal\taxonomy\TermInterface;

/**
 * Defines a class for building widget form elements and settings.
 */
trait HierarchicalSelectHelper {

  use TaxonomyStorage;

  /**
   * Defines the default settings for this plugin.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public static function defaultSettings(): array {
    $default_settings = parent::defaultSettings();
    $default_settings['hierarchical_select_ajax'] = [
      'hierarchy_depth' => 1,
      'label_level' => '',
      'description_level' => '',
      'none_label' => HierarchicalSelectElement::NONE_LABEL,
      'label_hide' => 0,
    ];
    return $default_settings;
  }

  /**
   * Returns a form to configure settings.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form definition for the settings.
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::settingsForm($form, $form_state);
    $settings = $this->getSetting('hierarchical_select_ajax');

    if (isset($element['placeholder'])) {
      $element['placeholder']['#access'] = FALSE;
    }
    if (isset($element['size'])) {
      $element['size']['#access'] = FALSE;
    }

    // Build hierarchical_select_ajax container element.
    $element['hierarchical_select_ajax'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
      '#tree' => TRUE,
    ];

    // Inherits hierarchical_select_ajax element.
    $hsa_element = &$element['hierarchical_select_ajax'];
    $hsa_element['hierarchy_depth'] = [
      '#min' => 1,
      '#type' => 'number',
      '#title' => $this->t('Hierarchy depth'),
      '#description' => $this->t('Limits the nesting level. Use 0 to display all values. For the hierarchy like "a" -> "b" -> "c" the selection of 2 will result in "b" being the deepest option.'),
      '#default_value' => $settings['hierarchy_depth'],
      '#required' => TRUE,
    ];
    $hsa_element['label_level'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label per level'),
      '#description' => $this->t('Enter labels for each hierarchy-level separated by comma.'),
      '#default_value' => $settings['label_level'],
      '#required' => TRUE,
    ];
    $hsa_element['description_level'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description per level'),
      '#default_value' => $settings['description_level'],
      '#description' => $this->t('Enter description per hierarchy-level separated by newline.'),
    ];
    $hsa_element['none_label'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The "no selection" label'),
      '#default_value' => $settings['none_label'],
      '#description' => $this->t('The label for an empty option per hierarchy-level separated by newline.'),
    ];
    $hsa_element['label_hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide field label'),
      '#default_value' => $settings['label_hide'] ?? FALSE,
      '#description' => $this->t('Hides the field label from the display.'),
    ];
    return $element;
  }

  /**
   * Returns a short summary for the settings.
   *
   * @return array
   *   A short summary of the settings.
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    $settings = $this->getSetting('hierarchical_select_ajax');
    $none = $this->t('None');
    $overriden = $this->t('Yes');

    // Remove size and placeholder summary.
    if (isset($summary[2])) {
      unset($summary[2]);
    }
    if (isset($summary[3])) {
      unset($summary[3]);
    }

    $summary[] = $this->t('Hierarchy depth: @hierarchy_depth', [
      '@hierarchy_depth' => $settings['hierarchy_depth'],
    ]);
    $summary[] = $this->t('Label level: @label_level', [
      '@label_level' => empty($settings['label_level']) ? $none : $settings['label_level'],
    ]);
    $summary[] = $this->t('Overriden description level: @description_level', [
      '@description_level' => empty($settings['description_level']) ? $none : $overriden,
    ]);
    $summary[] = $this->t('Overriden "no selection" label: @none_label', [
      '@none_label' => empty($settings['none_label']) ? $none : $overriden,
    ]);
    $summary[] = $this->t('Hide field label: @label_hide', [
      '@label_hide' => $settings['label_hide'] ?? 0,
    ]);

    return $summary;
  }

  /**
   * Setting per level getter.
   *
   * @param string $setting
   *   The level setting name.
   * @param string $level
   *   The level value.
   *
   * @return string
   *   Returns the string of the settings.
   */
  public function getSettingPerLevel(string $setting, string $level): string {
    $settings = $this->getSetting('hierarchical_select_ajax');
    $value = '';
    if ($setting === 'description_level') {
      $piece = explode(PHP_EOL, $settings['description_level']);
      $value = isset($piece[$level]) ? trim($piece[$level]) : '';
    }
    elseif ($setting === 'none_label') {
      $piece = explode(PHP_EOL, $settings['none_label']);
      $value = isset($piece[$level]) ? trim($piece[$level]) : HierarchicalSelectElement::NONE_LABEL;
    }
    elseif ($setting === 'label_level') {
      $piece = explode(',', $settings['label_level']);
      $value = isset($piece[$level]) ? trim($piece[$level]) : '';
    }
    return $value;
  }

  /**
   * Returns the form for a single widget.
   *
   * @return array
   *   The form elements for a single widget.
   */
  public function formElement(array &$element): array {
    $autocomplete = &$element['target_id'];
    $settings = $this->getSetting('hierarchical_select_ajax');
    $depth = $settings['hierarchy_depth'] ?? 0;
    $delta = $autocomplete['#delta'];

    $default_value = $autocomplete['#default_value'] instanceof TermInterface ? $autocomplete['#default_value']->id() : NULL;
    $parents = $this->getParents($default_value) ?? [];

    $element_id = $this->getElementIdByDelta($delta);
    $autocomplete['#weight'] = -1;
    $autocomplete['#id'] = 'hsa-autocomplete-' . $element_id;
    $autocomplete['#attributes']['class'][] = 'visually-hidden';

    // Build a HSA element container.
    $element['hierarchical_select_ajax'] = [
      '#type' => 'container',
      '#prefix' => '<div class="hsa-wrapper" id="hsa-' . $element_id . '">',
      '#suffix' => '</div>',
      '#attributes' => ['class' => ['hsa-container']],
    ];

    // Removes the element title from the display.
    if ($settings['label_hide']) {
      unset($autocomplete['#title']);
    }

    // Build a HSA elements storage.
    $element['hierarchical_select_ajax']['elements'] = [];
    $last_parent = $parents ? end($parents) : NULL;
    $hsa_element = &$element['hierarchical_select_ajax']['elements'];

    // Build initial form elements for each level.
    for ($level = 0; $level < $depth; $level++) {
      $hsa_element[$level] = $this->selectElement((string) $level, $delta, $parents ?? NULL, $default_value);
    }

    // Find last last element with parent to pre-populate options.
    foreach ($element['hierarchical_select_ajax']['elements'] as $level => $select) {
      $current_parent = $parents[$level] ?? NULL;
      if ($current_parent == $last_parent && isset($element['hierarchical_select_ajax']['elements'][$level + 1])) {
        $element['hierarchical_select_ajax']['elements'][$level + 1]['#options'] = $this->getChildrenOptions($last_parent, $level + 1);
      }
    }

    // Attach HSA styles.
    $element['#attached']['library'][] = 'hierarchical_select_ajax/style';

    return $element;
  }

  /**
   * Generates select element per level.
   *
   * @param string $level
   *   The element hierarchy level.
   * @param string $delta
   *   The element delta.
   * @param array $parents
   *   The taxonomy term parent IDs.
   * @param string|null $default_value
   *   The default value of the taxonomy term ID.
   *
   * @return array
   *   Returns the select element per level.
   */
  public function selectElement(string $level, string $delta, array $parents = [], string $default_value = NULL): array {
    $current_parent = $parents[$level] ?? NULL;

    $options = $this->getOptions((int) $level, $current_parent);
    $element_id = $this->getElementIdByLevel($level, $delta);

    // Load term parents.
    $parents = $this->getTermStorage()->loadParents($current_parent);

    // Replace options if parents exists.
    if ($parents) {
      $parent_new = reset($parents);
      $options = $this->getChildrenOptions($parent_new->id(), $level);
    }

    $element = [
      '#type' => 'select',
      '#title' => $this->getElementLabel($level),
      '#options' => $options,
      '#empty_option' => $this->getElementNoneLabel($level),
      '#description' => $this->getElementDescription($level),
      '#prefix' => '<div class="hsa-select" id="' . $element_id . '">',
      '#suffix' => '</div>',
      '#empty_value' => HierarchicalSelectElement::NONE_VALUE,
      '#default_value' => $current_parent,
      '#wrapper_attributes' => ['class' => ['hsa-select']],
      '#hsa_id' => $element_id,
      '#hsa_level' => $level,
      '#hsa_delta' => $delta,
      '#ajax' => [
        'callback' => [$this, 'selectElementOptionsCallback'],
        'wrapper' => $element_id,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading...'),
        ],
      ],

      // Fix an illegal choice issue.
      '#validated' => TRUE,
    ];

    return $element;
  }

  /**
   * Returns the list of options.
   *
   * @param string $level
   *   The hierarchy level.
   *
   * @return array
   *   Returns the widget options.
   */
  private function getOptions(string $level): array {
    $options = [];
    $handler_settings = $this->fieldDefinition->getSetting('handler_settings');

    if (!$handler_settings['target_bundles']) {
      return $options;
    }

    $vocabulary_storage = $this->getVocabularyStorage();
    $parent_options = $vocabulary_storage->getToplevelTids($handler_settings['target_bundles']);

    if ($level === "0") {
      foreach ($parent_options as $parent_tid) {
        $term = $this->getTerm($parent_tid);
        $options[$parent_tid]['name'] = $term->name->value;
        $options[$parent_tid]['weight'] = $term->weight->value;
      }
    }

    return $this->sortOptionsAlphabetically($options);
  }

  /**
   * Get the parent by taxonomy term ID.
   *
   * @param string $tid
   *   The parent taxonomy term ID.
   *
   * @return array
   *   The sorted array options.
   */
  public function getParents($tid) {
    $parents = $this->getTermStorage()->loadAllParents($tid);
    if (!$parents) {
      return $tid;
    }

    $parent_ids = [];
    foreach ($parents as $parent) {
      $parent_ids[] = $parent->id();
    }

    // Get the array keys.
    $option_keys = array_keys($parent_ids);

    // Get the array values.
    $option_values = array_values($parent_ids);

    // Filter array with elements in reverse order.
    $option_reverse = array_reverse($option_values);

    // Combine array keys and values.
    $options = array_combine($option_keys, $option_reverse);

    return $options;
  }

  /**
   * Load the term children by parent ID.
   *
   * @param string $parent_tid
   *   The parent taxonomy term ID.
   * @param string $level
   *   The hierarchy level.
   *
   * @return array
   *   The sorted array options.
   */
  public function getChildrenOptions($parent_tid, string $level = ''): array {
    $children = $this->getTermStorage()->loadChildren($parent_tid);
    $none = HierarchicalSelectElement::NONE_VALUE;

    // Set initial options.
    $options = [];
    $options[$none]['name'] = $this->getElementNoneLabel($level);
    $options[$none]['weight'] = -1000;

    if ($children) {
      foreach ($children as $child) {
        $tid = $child->id();
        $options[$tid]['name'] = $child->label();
        $options[$tid]['weight'] = $child->getWeight();
      }
    }

    return $this->sortOptionsAlphabetically($options);
  }

  /**
   * Sort array in alphabetical order.
   *
   * @param array $options
   *   The select options.
   *
   * @return array
   *   The sorted array options.
   */
  public function sortOptionsAlphabetically(array $options) {
    asort($options);
    $options = array_filter(array_combine(array_keys($options), array_column($options, 'name')));
    return $options;
  }

  /**
   * An options callback to update the selected options.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function selectElementOptionsCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Prevent messages to be shown after reloading the edit form,
    // all recent messages have to be deleted.
    \Drupal::messenger()->deleteAll();

    // Get the triggering element.
    $triggering_element = $form_state->getTriggeringElement();

    // Copy the original parents.
    // Remove the last level element as well.
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);

    $selected_level = $triggering_element['#hsa_level'];
    $delta = $triggering_element['#hsa_delta'];

    // Get the information as possible.
    $settings = $this->getSetting('hierarchical_select_ajax');
    $depth = $settings['hierarchy_depth'] ?? 0;
    $selected_tid = $triggering_element['#value'];
    $last_selected_tid = [];
    $none = $this->getElementNoneLabel($selected_level);

    // Pre-populates the next element options.
    for ($idx = 0; $idx < $depth; $idx++) {
      $current_level = $idx;
      $element_parents = [];
      $element_parents = $parents;

      // Update and get the next nested element array.
      if ($selected_level < $current_level) {
        array_push($element_parents, $current_level);
        $next = NestedArray::getValue($form, $element_parents);
        $next['#options'] = $this->getChildrenOptions($selected_tid, $current_level);
        $next['#value'] = NULL;
        $next['#attributes']['disabled'] = $selected_level == 0 && $current_level > 1;
        $response->addCommand(new ReplaceCommand('#' . $this->getElementIdByLevel($current_level, $delta), $next));
      }

      // Update and get the previous nested element array.
      if ($selected_level <= $current_level) {
        array_push($element_parents, $current_level - 1);
        $prev = NestedArray::getValue($form, $element_parents);
        if ($prev && $selected_tid == $none) {
          $last_selected_tid[] = $prev['#value'];
        }
      }
    }

    if ($last_selected_tid) {
      $last_selected_tid = array_filter($last_selected_tid);
    }

    // Get the selected taxonomy term for autocomplete field.
    $tid = $last_selected_tid[0] ?? $selected_tid;
    $term = $this->getTerm($tid);
    $selected = NULL;

    // Overrides selected if term is valid term object.
    if ($term && $term instanceof TermInterface) {
      $selected = $term->label() . ' (' . $term->id() . ')';
    }

    // Replace the autocomplete value on option selection.
    $response->addCommand(new InvokeCommand('input#hsa-autocomplete-' . $this->getElementIdByDelta($delta), 'val', [$selected]));

    return $response;
  }

  /**
   * Generates ID for each element by delta.
   *
   * @param string $delta
   *   The element delta.
   *
   * @return string
   *   Returns the generated element ID.
   */
  public function getElementIdByDelta(string $delta) {
    $field_definition = $this->fieldDefinition;
    $id = str_replace('.', '_', $field_definition->id());
    $id .= '-value-' . $delta;
    return Html::getId($id);
  }

  /**
   * Generates ID for each element by level.
   *
   * @param string $level
   *   The element hierarchy level.
   * @param string $delta
   *   The element delta.
   *
   * @return string
   *   Returns the generated element ID.
   */
  public function getElementIdByLevel(string $level, string $delta) {
    $field_definition = $this->fieldDefinition;
    $field_id = str_replace('.', '_', $field_definition->id()) . '__' . 'value-' . $delta . '__' . 'level-' . $level;
    $element_id = 'hsa-select-element__' . $field_id;
    return Html::getId($element_id);
  }

  /**
   * Get the element label by level.
   *
   * @param string $level
   *   The element hierarchy level.
   *
   * @return string|null
   *   Returns the element label.
   */
  public function getElementLabel(string $level) {
    $value = $this->getSettingPerLevel('label_level', $level);
    if ($value) {
      return Xss::filter($value);
    }
  }

  /**
   * Get the element description by level.
   *
   * @param string $level
   *   The element hierarchy level.
   *
   * @return string|null
   *   Returns the description label.
   */
  public function getElementDescription(string $level) {
    $value = $this->getSettingPerLevel('description_level', $level);
    if ($value) {
      return Xss::filterAdmin($value);
    }
  }

  /**
   * Get the element "none_label" by level.
   *
   * @param string $level
   *   The element hierarchy level.
   *
   * @return string|null
   *   Returns the "none_label" label.
   */
  public function getElementNoneLabel(string $level) {
    $value = $this->getSettingPerLevel('none_label', $level);
    if ($value) {
      return Xss::filter($value);
    }
  }

}
