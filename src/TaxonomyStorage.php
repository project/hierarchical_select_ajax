<?php

namespace Drupal\hierarchical_select_ajax;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\VocabularyStorageInterface;

/**
 * The container for representing the `hierarchical_select_ajax` storage.
 */
trait TaxonomyStorage {

  /**
   * An instance of the "entity_type.manager" service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Returns the entity storage.
   *
   * @param string $entity_type
   *   The entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage.
   */
  private function getStorage($entity_type): EntityStorageInterface {
    if (NULL === $this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::service('entity_type.manager');
    }

    return $this->entityTypeManager->getStorage($entity_type);
  }

  /**
   * Returns storage of the "taxonomy_vocabulary" entities.
   *
   * @return \Drupal\taxonomy\VocabularyStorageInterface
   *   The storage of the "taxonomy_vocabulary" entities.
   */
  protected function getVocabularyStorage(): VocabularyStorageInterface {
    return $this->getStorage('taxonomy_vocabulary');
  }

  /**
   * Returns storage of the "taxonomy_term" entities.
   *
   * @return \Drupal\taxonomy\TermStorageInterface
   *   The storage of the "taxonomy_term" entities.
   */
  protected function getTermStorage(): TermStorageInterface {
    return $this->getStorage('taxonomy_term');
  }

  /**
   * Returns storage of the "taxonomy_term" entities.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The storage of the "taxonomy_term" entities.
   */
  protected function getTerm(string $tid) {
    return $this->getStorage('taxonomy_term')->load($tid);
  }

}
